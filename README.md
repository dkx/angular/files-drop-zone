# DKX/Angular/FilesDropZone

Files drop zone directive for angular

## Installation

```bash
$ npm install --save @dkx/ng-files-drop-zone
```

or with yarn

```bash
$ yarn add @dkx/ng-files-drop-zone
```

## Usage

**AppModule:**

```typescript
import {NgModule} from '@angular/core';
import {FilesDropZoneModule} from '@dkx/ng-files-drop-zone';

@NgModule({
    imports: [
        FilesDropZoneModule,
    ],
})
export class AppModule {}
```

**Template:**

```html
<div (dkxFilesDropZone)="filesDropped($event)" style="width: 500px; height: 300px; border: 1px dashed black;"></div>
```

**Component:**

```typescript
import {Component} from '@angular/core';

@Component({
    templateUrl: './form.component.html',
})
export class FormComponent
{
    public filesDropped(files: Array<File>): void
    {
        console.log(files);
    }
}
```

## Styling

You can use predefined CSS classes to style the drop zone:

* `dkx-files-drop-zone-default`: Not dragging any files over the drop zone
* `dkx-files-drop-zone-dragging`: Dragging files over the drop zone
* `dkx-files-drop-zone-pristine`: Default state, no files dropped
* `dkx-files-drop-zone-dirty`: Some files were dropped
