import {Directive, EventEmitter, HostBinding, HostListener, Output} from '@angular/core';


@Directive({
	selector: '[dkxFilesDropZone]',
})
export class FilesDropZoneDirective
{


	@Output('dkxFilesDropZone')
	public dropped: EventEmitter<any> = new EventEmitter;

	private _dirty: boolean = false;

	private _dragging: boolean = false;


	public get dirty(): boolean
	{
		return this._dirty;
	}


	public get dragging(): boolean
	{
		return this._dragging;
	}


	@HostBinding('class.dkx-files-drop-zone-pristine')
	public get classPristine(): boolean
	{
		return !this._dirty;
	}


	@HostBinding('class.dkx-files-drop-zone-dirty')
	public get classDirty(): boolean
	{
		return this.dirty;
	}


	@HostBinding('class.dkx-files-drop-zone-dragging')
	public get classDragging(): boolean
	{
		return this._dragging;
	}


	@HostBinding('class.dkx-files-drop-zone-default')
	public get classDefault(): boolean
	{
		return !this._dragging;
	}


	@HostListener('drop', ['$event'])
	public onDrop(e: DragEvent): void
	{
		e.preventDefault();

		const files: Array<File> = [];

		if (e.dataTransfer.items) {
			for (let i = 0; i < e.dataTransfer.items.length; i++) {
				if (e.dataTransfer.items[i].kind === 'file') {
					const file = e.dataTransfer.items[i].getAsFile();
					if (file) {
						files.push(file);
					}
				}
			}
		} else {
			for (let i = 0; i < e.dataTransfer.files.length; i++) {
				files.push(e.dataTransfer.files[i]);
			}
		}

		this._dirty = files.length > 0;
		this._dragging = false;

		this.dropped.emit(files);
	}


	@HostListener('dragover', ['$event'])
	public onDragOver(e: DragEvent): void
	{
		this._dragging = true;
		e.preventDefault();
	}


	@HostListener('dragleave')
	public onDragExit(): void
	{
		this._dragging = false;
	}


	public clear(): void
	{
		this._dirty = false;
		this._dragging = false;
	}

}
