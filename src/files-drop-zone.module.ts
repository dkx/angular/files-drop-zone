import {NgModule} from '@angular/core';
import {FilesDropZoneDirective} from './files-drop-zone.directive';


@NgModule({
	declarations: [
		FilesDropZoneDirective,
	],
	exports: [
		FilesDropZoneDirective,
	],
})
export class FilesDropZoneModule {}
